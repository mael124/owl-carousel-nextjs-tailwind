import Head from 'next/head'

var $ = require('jquery');
if (typeof window !== 'undefined') {
   window.$ = window.jQuery = require('jquery');
}
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import dynamic from 'next/dynamic';
const OwlCarousel = dynamic(() => import('react-owl-carousel'), {
  ssr: false,
});

{/*
	-	installer Owl-carousel : npm i react-owl-carousel
	-	installer Jquery : npm i jquery
	-	dans le fichier next.config.js :
		* importer webpack avant la function nextConfid : const webpack = require("webpack");
		* ensuite ajouter cette funtion dans la function nextConfig:
			webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
				config.plugins.push(
				new webpack.ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery',
				'window.jQuery': 'jquery',
				}))
				return config;
			}
		(Voir le fichier next.config.js)

	-	Tout en haut de fichier ou se trouve ton component OwlCarousel:
		* Ajouter ce code si vous avez une erreur de "window is not defined" :
			var $ = require('jquery');
			if (typeof window !== 'undefined') {
				window.$ = window.jQuery = require('jquery');
			}
		
		* ensuite importer les fichiers styles de owl-carousel:
			import 'owl.carousel/dist/assets/owl.carousel.css';
			import 'owl.carousel/dist/assets/owl.theme.default.css';
		
		*enfin importer le module owl-carousel en utilisant next.js dynamic import comme ceci:
			import dynamic from 'next/dynamic';
			const OwlCarousel = dynamic(() => import('react-owl-carousel'), {
			ssr: false,
			});

	-	pour la custimisation du owl carousel voir le fichier global.css
	- 	et pour la declaration du carousel dans le html voir le component "OwlCarousel" qui se trouve juste en bas du fichier dans lequel nous nous trouvons
	- pour plus d'info sur les differentes options du Owl Carousel : https://www.npmjs.com/package/react-owl-carousel

*/}

export default function Home() {

	const testimonials = [
        {
            id:1,
			name:'Bah Ismaila',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum tincidunt arcu, quis congue justo auctor nec. Proin faucibus, ante non auctor vestibulum, est erat interdum dolor, et venenatis nibh arcu ac est. Curabitur eget iaculis augue. In hac habitasse platea dictumst. Curabitur ac arcu sit amet felis pellentesque auctor.'

        },
        {
            id:2,
			name:'Anvi',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum tincidunt arcu, quis congue justo auctor nec. Proin faucibus, ante non auctor vestibulum, est erat interdum dolor, et venenatis nibh arcu ac est. Curabitur eget iaculis augue. In hac habitasse platea dictumst. Curabitur ac arcu sit amet felis pellentesque auctor.'

        },
    ]

	// Gestion du responsive des items du carousel
    const responsive = {
            0:{
                items:1
            },
            600:{
                items:2
            },
            992:{
                items:3
            },
            1000:{
                items:4
            },
            1440:{
                items:1
            }
        }
  return (
	<>
		<Head>
			<title>title page</title>
		</Head>
		<div className='px-10 py-5 rounded-sm bg-white shadow-sm max-w-4xl mx-auto mt-10'>
  			{/* Component OwlCarousel */}
			<OwlCarousel 
				className="owl-carousel owl-theme" 
				loop 
				margin={10} 
				items={4} 
				autoplay 
				autoplayTimeout={4000} 
				autoplayHoverPause 
				smartSpeed={2000}
				responsive={responsive}
				nav
			>
				{
					testimonials.map((item) =>(
						<div key={item.id} className="item max-w-xl text-center">
							<p className='text-gray-500 text-sm mb-5'>{item.description}</p>
							<h4 className='text-lg font-bold'>{item.name}</h4>
						</div>
					))
				}
			</OwlCarousel>
		</div>
		
	</>
  )
}
